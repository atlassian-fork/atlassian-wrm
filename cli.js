#!/usr/bin/env node

// Command line support to get you start quickly. You probably would
// like to integrate compiler with your node.js build tools, use this script as an example.

// Helpers.
var cli = require('commander');
var compile = require('./compile').compile;
var _ = require('./support/underscore');
var fsPath = require('path');
var ATLASSIAN_PLUGIN_XML = 'atlassian-plugin.xml';
function onError (err) {
    console.error(err.stack || err);
    process.exit(1);
}

// Parsing arguments.
var defaultInputDir = './src/main/resources';
var defaultTargetDir = './target';
cli._name = 'wrm-compile';
cli
    .usage(' <directory with the `atlassian-plugin.xml`, default is `' + defaultInputDir + '`>')
    .option('-t, --target', 'target directory, default is `' + defaultTargetDir + '`')
    .parse(process.argv);

var inputDir = fsPath.resolve(process.argv.slice[2] || defaultInputDir);
var targetDir = fsPath.resolve(cli.target || defaultTargetDir);

var atlassianPluginXmlPath = fsPath.join(inputDir, ATLASSIAN_PLUGIN_XML);
var outputDir = fsPath.join(targetDir, "classes");

// Compiling.
compile(atlassianPluginXmlPath, outputDir, _.fork(onError, function () {
    console.info("AMD modules for \"" + atlassianPluginXmlPath + "\" successfully compiled.");
}));