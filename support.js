var fs = require('fs');
var fsPath = require('path');
var _ = require('./support/underscore');
var p = exports.p = console.log.bind(console);

// List files in directory.
var _listFiles = function _listFiles (dir, cb) {
    var results = [];
    fs.readdir(dir, _.fork(cb, function(list) {
        var i = 0;
        (function next () {
          var file = list[i++];
          if (!file) return cb(null, results);
          file = fsPath.join(dir, file);
          fs.stat(file, function(err, stat) {
            if (stat && stat.isDirectory()) {
              _listFiles(file, function(err, res) {
                results = results.concat(res);
                next();
              });
            } else {
              results.push(file);
              next();
            }
          });
        })();
    }));
};
exports.listFiles = function (dir, cb) {
    _listFiles(dir, _.fork(cb, function (filePaths) {
        cb(null, filePaths.map(function (filePath) {
            return filePath.replace(dir + fsPath.sep, '')
        }));
    }));
};

// Normalizes relative module path and makes it absolute against some base path.
exports.normalisePath = function normalisePath (absoluteBasePath, currentDirPath, path) {
    if (/^\//.test(currentDirPath)) throw new Error("current dir path should be relative but it's \"" + currentDirPath + "\"!");
    if (!/^\//.test(absoluteBasePath)) throw new Error('base path should be absolute!');
    if (/^\//.test(path)) throw new Error("invalid path `" + path + "`, absolute paths not supported!");
    if (/^\./.test(path)) {
        var absolutePath = fsPath.join(absoluteBasePath, currentDirPath, path);
        var prefix = absoluteBasePath + fsPath.sep;
        if (absolutePath.indexOf(prefix) != 0) {
            throw new Error("file " + path + " not exists!")
        }
        return absolutePath.replace(prefix, '');
    } else return path;
};